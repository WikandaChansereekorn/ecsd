import guess_number
import unittest
from unittest import mock


class test_guess_number(unittest.TestCase):
    
    @mock.patch('random.randint', return_value=int(5))
    def test_guess_int_1_to_9_should_be_5(self, random_randint):
        result = guess_number.guess_int(1, 9)
        self.assertEqual(result, 5, "should be 5")
        random_randint.assert_called_once_with(1, 9)

    @mock.patch('random.randint', return_value=int(25))
    def test_guess_int_10_to_45_should_be_25(self, random_randint):
        result = guess_number.guess_int(10, 45)
        self.assertEqual(result, 25, "should be 25")
        random_randint.assert_called_once_with(10, 45)

    @mock.patch('random.randint', return_value=int(78))
    def test_guess_int_11_to_88_should_be_78(self, random_randint):
        result = guess_number.guess_int(11, 88)
        self.assertEqual(result, 78, "should be 78")
        random_randint.assert_called_once_with(11, 88)

    @mock.patch('random.randint', return_value=int(-15))
    def test_guess_int_minus25_to_0_should_be_minus15(self, random_randint):
        result = guess_number.guess_int(-25, 0)
        self.assertEqual(result, -15, "should be -15")
        random_randint.assert_called_once_with(-25, 0)

    @mock.patch('random.randint', return_value=int(0))
    def test_guess_int_minus50_to_0_should_be_0(self, random_randint):
        result = guess_number.guess_int(-50, 0)
        self.assertEqual(result, 0, "should be 0")
        random_randint.assert_called_once_with(-50, 0)

    @mock.patch('random.randint', return_value=int(-1))
    def test_guess_int_minus99_to_99_should_be_minus1(self, random_randint):
        result = guess_number.guess_int(-99, 99)
        self.assertEqual(result, -1, "should be -1")
        random_randint.assert_called_once_with(-99, 99)

   


    @mock.patch('random.uniform', return_value=float(1.25))
    def test_guess_float_1dot10_to_10_should_be_1dot25(self, random_randint):
        result = guess_number.guess_float(1.10, 10)
        self.assertEqual(result, 1.25, "should be 1.25")
        random_randint.assert_called_once_with(1.10, 10)

    @mock.patch('random.uniform', return_value=float(4.08))
    def test_guess_float_2_to_20_should_be_4dot08(self, random_randint):
        result = guess_number.guess_float(2, 20)
        self.assertEqual(result, 4.08, "should be 4.08")
        random_randint.assert_called_once_with(2, 20)

    @mock.patch('random.uniform', return_value=float(99.98))
    def test_guess_float_59dot99_to_99dot99_should_be_99dot98(self, random_randint):
        result = guess_number.guess_float(59.99, 99.99)
        self.assertEqual(result, 99.98, "should be 99.98")
        random_randint.assert_called_once_with(59.99, 99.99)

    @mock.patch('random.uniform', return_value=float(555.1234))
    def test_guess_float_75dot89_to_643dot321_should_be_555dot1234(self, random_randint):
        result = guess_number.guess_float(75.89, 643.321)
        self.assertEqual(result, 555.1234, "should be 555.1234")
        random_randint.assert_called_once_with(75.89, 643.321)

    @mock.patch('random.uniform', return_value=float(-1.222))
    def test_guess_float_minus5_to_10_should_be_minus1dot222(self, random_randint):
        result = guess_number.guess_float(-5, 10)
        self.assertEqual(result, -1.222, "should be -1.222")
        random_randint.assert_called_once_with(-5, 10)

    @mock.patch('random.uniform', return_value=float(-88.732))
    def test_guess_float_minus999_to_999_should_be_minus88dot732(self, random_randint):
        result = guess_number.guess_float(-999, 999)
        self.assertEqual(result, -88.732, "should be -88.732")
        random_randint.assert_called_once_with(-999, 999)

    @mock.patch('random.uniform', return_value=float(0.99))
    def test_guess_float_0_to_1_should_be_0dot99(self, random_randint):
        result = guess_number.guess_float(0, 1)
        self.assertEqual(result, 0.99, "should be 0.99")
        random_randint.assert_called_once_with(0, 1)
